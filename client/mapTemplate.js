var LatLon = require('geodesy').LatLonSpherical;

// TODO: get user input for either duration or distance
Template.mapTemplate.onRendered(function () {
	// Currently using Google's API key from their examples
	$.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyDDP1jfwBysdT5bAPnpRdYBQ1oIw5oDf7c&libraries=places",
		function (data, textStatus, jqxhr) {
			//console.log( data ); // Data returned
			//console.log( textStatus ); // Success
			//console.log( jqxhr.status ); // 200
			console.log("Google Maps Javascript API loaded.");
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function (position) {
					var origin = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
					console.log($('#map-container')[0]);
					map = new google.maps.Map($('#map-container')[0], {
						center: origin,
						zoom: 15,
					});
					
					new google.maps.Marker({
						position: origin,
						map: map,
					});
					
					// TODO: Should be creating 4 circles here by creating a center point
					// that's north, south, east and west
					// Calculate the destination (center of the circle) and convert to Google LatLng object
					var start = new LatLon(position.coords.latitude, position.coords.longitude);
					var distance = 750;
					var bearing = 90;
					var center = start.destinationPoint(distance, bearing);
					var centerLatLng = new google.maps.LatLng({
						lat: center.lat,
						lng: center.lon
					});
					new google.maps.Marker({
						position: centerLatLng,
						map: map,
					});
					
					var circle = new google.maps.Circle({
						strokeColor: '#FF0000',
						strokeOpacity: 0.8,
						strokeWeight: 2,
						fillColor: '#FF0000',
						fillOpacity: 0.35,
						map: map,
						center: centerLatLng,
						radius: distance
					});
					
					
					// Start with north south east west
					// then add midpoints on circle edge (northwest northeast)
					// compare duration with desired travel time. If not within a few minutes, continue
					// if duration is way off, shrink or increase radius
					// then use 4 points with a degree shift value (-20 degrees, plus 50 degrees)
					// then use 8 points (4 points w/ midpoints) with a degree shift
					
					// After finding two good loups, calculate the duration to radius of circle used
			
					
					
					// Calculate some points on the circle
					var north = center.destinationPoint(distance, 0);
					north = new google.maps.LatLng({
						lat: north.lat,
						lng: north.lon
					});
					new google.maps.Marker({
						position: north,
						map: map,
					});
					
					var northwest = center.destinationPoint(distance, 315);
					northwest = new google.maps.LatLng({
						lat: northwest.lat,
						lng: northwest.lon
					});
					new google.maps.Marker({
						position: northwest,
						map: map,
					});
					
					var northeast = center.destinationPoint(distance, 45);
					northeast = new google.maps.LatLng({
						lat: northeast.lat,
						lng: northeast.lon
					});
					new google.maps.Marker({
						position: northeast,
						map: map
					});
					
					var south = center.destinationPoint(distance, 180);
					south = new google.maps.LatLng({
						lat: south.lat,
						lng: south.lon
					});
					new google.maps.Marker({
						position: south,
						map: map,
					});
					
					var southeast = center.destinationPoint(distance, 125);
					southeast = new google.maps.LatLng({
						lat: southeast.lat,
						lng: southeast.lon
					});
					new google.maps.Marker({
						position: southeast,
						map: map,
					});
					
					var southwest = center.destinationPoint(distance, 225);
					southwest = new google.maps.LatLng({
						lat: southwest.lat,
						lng: southwest.lon
					});
					new google.maps.Marker({
						position: southwest,
						map: map,
					});
					
					var east = center.destinationPoint(distance, 90);
					east = new google.maps.LatLng({
						lat: east.lat,
						lng: east.lon
					});
					new google.maps.Marker({
						position: east,
						map: map,
					});
					
					// TODO: only compare by distance right now. Try to compare by loupiness. https://developers.arcgis.com/javascript/3/jsapi/esri.geometry.geometryengine-amd.html#difference
					// TODO: reverse the waypoint order
					// TODO: automate by increasing by degrees
					// TODO: time the calculations
					// TODO: use meteor methods and compare time between client/server
					var waypoints = [
						{location: northwest, stopover: false},
						{location: north, stopover: false},
						{location: northeast, stopover: false},
						{location: east, stopover: false},
						{location: southeast, stopover: false},
						{location: south, stopover: false},
						{location: southwest, stopover: false}];
					
					
					var directionsService = new google.maps.DirectionsService;
					var directionsDisplay = new google.maps.DirectionsRenderer;
					directionsDisplay.setMap(map);
					
					directionsService.route({
						origin: origin,
						destination: origin,
						waypoints: waypoints,
						provideRouteAlternatives: true,
						avoidHighways: true,
						avoidTolls: true,
						travelMode: 'BICYCLING',
					}, function (response, status) {
						if (status === 'OK') {
							console.log(response);
							directionsDisplay.setDirections(response);
							var route = response.routes[0];
							
						} else {
							window.alert('Directions request failed due to ' + status);
						}
					});
					
					
				});
			} else {
				console.log("can't use HTML5 geolocation");
			}
			
			
		});
});